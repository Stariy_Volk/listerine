module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '_src/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'img/'
                }]
            }
        },

        concat: {
            // 2. Настройка для объединения файлов находится тут
            jsC: {
                src: [
                    '_src/js/*.js',
                    '_src/js/modules/*.js'
                ],
                dest: 'js/main.js'
            },
            cssC : {
                src: [
                    '_src/css/normalize.min.css',
                    '_src/css/style.css',
                    '_src/css/*.css'
                ],
                dest: 'css/style.css'
            }
        },

        uglify: {
            build: {
                src: 'js/main.js',
                dest: 'js/main.min.js'
            }
        },

        less: {
            development: {
                options: {
                    paths: ['_src/css/less'],
                    compress: true,
                    sourceMap: true,
                    sourceMapFilename: 'css/main.css.map', // where file is generated and located
                    sourceMapURL: '/css/main.css.map', // the complete url and filename put in the compiled css file
                    sourceMapBasepath: '/', // Sets sourcemap base path, defaults to current working directory.
//                    sourceMapRootpath: 'file:///C:/OpenServer/domains/samara/' // adds this path onto the sourcemap filename and less file paths
                    sourceMapRootpath: '/' // adds this path onto the sourcemap filename and less file paths
                },
                files: {
                    "_src/css/style.css": '_src/css/style.less'
                }
            }
        },

        watch: {
            options: {
                    livereload: true
            },
//            js: {
//                files: ['_src/js/*.js'],
//                tasks: ['concat:jsC', 'uglify'],
//                options: {
//                    livereload: true
//                }
//            },
            js: {
                files: ['_src/js/*.js','_src/js/modules/*.js'],
                tasks: ['concat:jsC']
            },
            less: {
                files: ['_src/css/*.less','_src/css/less/*.less'],
                tasks: ['less','concat:cssC']
            },
            html: {
                files: ['html/*.html'],
                tasks: []
            }
//            img: {
//                files: ['_src/img/**/*.{png,jpg,gif}','_src/img/*'],
//                tasks: ['imagemin']
//            }
        }

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-notify');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('cssBuild', ['less', 'concat:cssC']);
    grunt.registerTask('jsBuild', ['concat:jsC']);
    grunt.registerTask('build', ['concat:jsC', 'uglify', 'imagemin', 'less', 'cssBuild']);
    grunt.registerTask('preBuild', ['concat:jsC', 'imagemin', 'less', 'cssBuild']);

    grunt.registerTask('default', ['concat:jsC', 'imagemin', 'less', 'cssBuild']);

};