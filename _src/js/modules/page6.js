/**
 * Created by Verdi on 07.12.2014.
 */

/**
 * Created by Verdi on 07.12.2014.
 */

//---------------page6 script BEGIN--------------//

var loaderApp6 = function(){
    var base = $(".page6 .page6_loader"),
        top = base.find(".page5_loader_top span"),
        bottom = base.find(".page5_loader_bottom span"),
        counter = base.find(".page6_loader_text span"),
        count = parseInt(counter.text()),
        workCount = count;

    function choseSide(){
        if(count <= 50){
            workCount = count;
            return top
        } else {
            workCount = count-50;
            return bottom
        }
    }

    function changeCSS(){
        var elem = choseSide(),
            degree = 180/50*workCount;
        if(elem == bottom){
            elem.css({
                "-webkit-transform": "rotate(180deg)",
                "transform": "rotate(180deg)"
            });
        }
        elem.css({
            "-webkit-transform": "rotate("+degree+"deg)",
            "transform": "rotate("+degree+"deg)"
        });
    }

    return {
        init: function(base){

        },
        countTo: function(num){
            var countBlock = $(".page5_loader_wrap");
            setTimeout(function(){
                counter.removeClass("counting");
            }, 2000);
            if(!counter.hasClass("counting")){
                counter.addClass("counting");
                makeCount(count, num, 2000/(num-count), function(next){
                    count = next;
                    counter.text(count);
                    changeCSS();
                    if(count==100){
                        countBlock.addClass("full");
                    } else {
                        countBlock.removeClass("full");
                    }
                });
            }
        }
    }
};

(function(){
    loaderApp6().init();


})();

//---------------page6 script END--------------//
