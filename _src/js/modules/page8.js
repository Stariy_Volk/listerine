/**
 * Created by Verdi on 08.12.2014.
 */

//---------------page8 script BEGIN--------------//

(function () {
    $(".page8_cat .page8_buttons_link").on("click", function(e){
        e.preventDefault();
        var elem = $(this),
            href = elem.attr("href");
        if(href=='#cat'){
            $(".page8_cat.active").addClass("cat_final");
            $(".page9_cat").addClass("active");
            setTimeout(function(){
                $(".page10 .page_bg").trigger("click");
            },800);
        } else {
            $(".page9_box").addClass("active");
            $(".page10 .page_bg").trigger("click");
        }

    });
})();

//---------------page8 script END--------------//
