/**
 * Created by Verdi on 07.12.2014.
 */

//---------------page5 script BEGIN--------------//

var loaderApp5 = function(){
    var base = $(".page5 .page5_loader"),
        top = base.find(".page5_loader_top span"),
        bottom = base.find(".page5_loader_bottom span"),
        counter = base.find(".page5_loader_text span"),
        count = parseInt(counter.text()),
        workCount = count;

    function choseSide(){
        if(count <= 50){
            workCount = count;
            return top
        } else {
            workCount = count-50;
            return bottom
        }
    }

    function changeCSS(){
        var elem = choseSide(),
            degree = 180/50*workCount;
        elem.css({
            "-webkit-transform": "rotate("+degree+"deg)",
            "transform": "rotate("+degree+"deg)"
        });
    }

    return {
        init: function(base){

        },
        countTo: function(num){
            setTimeout(function(){
                counter.removeClass("counting");
            }, 1000);
            if(!counter.hasClass("counting")){
                counter.addClass("counting");
                makeCount(count, num, 1000/(num-count), function(next){
                    count = next;
                    counter.text(count);
                    changeCSS();
                });
            }
        }
    }
};

function makeCount(start, top, time, callback){
    var next = start+1;
    if(next <= top){
        if(callback){
            callback(next);
        }
        setTimeout(function(){
            makeCount(next, top, time, callback);
        }, time);
    } else if(top < start){
        next = top;
        if(callback){
            callback(next);
        }
        setTimeout(function(){
            makeCount(next, top, time, callback);
        }, time);
    }
}

(function(){
    loaderApp5().init();


})();

//---------------page5 script END--------------//
