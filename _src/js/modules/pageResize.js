//-----------------------------Ресайз всея begin
(function(){
    function pageResize(data){
        if(typeof data.normalSize == "undefined"){
            data.normalSize = {
                width: 1400,
                height: 900
            }
        }
        if(typeof data.normalFont == "undefined"){
            data.normalFont = 12;
        }
        var win = data.wrapper,
            winWidth = win.width(),
            winHeight = win.height(),
            normalWidth = data.normalSize.width,
            normalHeight = data.normalSize.height,
            ratio = normalWidth/normalHeight,
            normalSize = data.normalFont,
            perc = 1,
            elem = data.resizeElem;
        $(window).resize(function(){
            winWidth = win.width();
            winHeight = win.height();
            var tmpHeight = 0,
                tmpWidth = 0;
            if(ratio < winWidth/winHeight){
                tmpHeight = winHeight;
                tmpWidth = tmpHeight * ratio;

            } else {
                tmpWidth = winWidth;
                tmpHeight = tmpWidth / ratio;
            }
            perc = tmpWidth/normalWidth;
            var newSize = normalSize*perc;
            elem.css({
                fontSize: newSize,
                width: winWidth,
                height: winHeight
            });
            //$("viewsize").height(winHeight).find("div").height($(document).height());
        });
        $(window).trigger("resize");
    }

    pageResize({
        wrapper: $("body"),
        resizeElem: $(".maincontent"),
        normalSize: {
            width: 1920,
            height: 1080
        },
        normalFont: 100
    });

})();

//-----------------------------Ресайз всея end


