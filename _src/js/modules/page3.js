/**
 * Created by Verdi on 06.12.2014.
 */

//---------------page3 script BEGIN--------------//

(function () {
    $(".page3_icon_hover").on("mouseenter", function(){
        $(this).addClass("animated rotateIn");
    }).on("mouseleave", function(){
        $(this).removeClass("rotateIn");
    });
})();

//---------------page3 script END--------------//
