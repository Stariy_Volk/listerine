/**
 * Created by Verdi on 08.12.2014.
 */

//---------------page9 script BEGIN--------------//

var page9App = function(){
    var count = 0,
        counter = $(".page9 .page7_count_num span"),
        countLine = $(".page9 .page7_count"),
        counterFill = $(".page9 .page7_shape_fill");

    function changeCSS(){
        counter.text(count);
        countLine.css({
            bottom: count+"%"
        });
        counterFill.css({
            bottom: (count-100)+"%"
        });
    }

    return {
        init: function(){

        },
        countTo: function(num){
            counter = $(".page9>.active .page7_count_num span");
            countLine = $(".page9>.active .page7_count");
            counterFill = $(".page9>.active .page7_shape_fill");
            count = parseInt(counter.text());
            if(!counter.hasClass("counting")){
                counter.addClass("counting");
                makeCount(count, num, 6000/(num-count), function(next){
                    count = next;
                    counter.text(count);
                    changeCSS();
                    if(count==100){
                        setTimeout(function(){
                            $(".page9").addClass("loaded");
                        }, 400);
                        counter.removeClass("counting");
                    } else {
                        $(".page9").removeClass("loaded");
                    }
                });
            }
        }
    }
};

(function () {
    page9App().init();
})();

//---------------page9 script END--------------//
