/**
 * Created by Verdi on 06.12.2014.
 */

//---------------navigation script BEGIN--------------//



var navApp = function(){
    var base = $(".maincontent"),
        pages = $(".maincontent > section"),
        step = base.height(),
        activePage = 0,
        pageTime = 600,
        pageState = [];

    setTimeout(function(){pages.eq(0).addClass("active");}, pageTime);

    function movePage(delta, callback){
        var newActive = 0;
        if(delta > 0){
            newActive = activePage+1;
        } else {
            newActive = activePage-1;
        }
        if((newActive < pages.length) && (newActive >= 0)){
            if(newActive == 3){
                if($(".page4 > .active").length){
                    openPage(newActive, activePage);
                }
            } else if(newActive == 8){
                if($(".page9 > .active").length){
                    openPage(newActive, activePage);
                }
            } else {
                openPage(newActive, activePage);
            }
        }
        setTimeout(function(){
            callback();
        }, pageTime);
    }

    function openPage(num, lastNum) {
        if(!(num == 0 && activePage == 0)){
            base.animate({top: -pages.eq(num).position().top}, pageTime, function () {
                var states = '';
                pages.eq(num).addClass("active");
                pages.eq(lastNum).removeClass("active");
                activePage = num;
                for (var i = 0; i < pageState.length; i++) {
                    states += '/' + pageState[i];
                    $("#" + pageState[i]).addClass('active');
                }
                if(activePage < 8){
                    document.location.hash = "page_" + activePage + states;
                }
            });
            if(num == 4){
                setTimeout(function(){
                    loaderApp5().countTo(25);
                }, 2000);
            } else {
                setTimeout(function(){
                    loaderApp5().countTo(0);
                }, 300);
            }
            if(num == 5){
                setTimeout(function(){
                    loaderApp6().countTo(100);
                }, 2000);
            } else {
                setTimeout(function(){
                    loaderApp6().countTo(0);
                }, 300);
            }
            if(num == 6){
                setTimeout(function(){
                    page6App().countTo(100);
                }, 2000);
            } else {
                setTimeout(function(){
                    page6App().countTo(0);
                }, 300);
            }
            if(num == 8){
                if($(".page9_box.active").length){
                    setTimeout(function(){
                        page9App().countTo(100);
                    }, 4000);
                } else {
                    setTimeout(function(){
                        page9App().countTo(100);
                    }, 4000);
                }
            } else {
                setTimeout(function(){
                    page9App().countTo(0);
                }, 300);
            }
            if(num == 0){
                $('.page_next').fadeOut();
            } else {
                $('.page_next').fadeIn();
            }
            if(num == pages.length - 1){
                $('.page_prev').fadeOut();
            } else {
                $('.page_prev').fadeIn();
            }
        }
    }

    function loadHash(){
        var hash = document.location.hash.replace("#",""),
            hashParts = hash.split("/"),
            page = parseInt(hashParts[0].replace("page_",""));
        if(hashParts.length > 1){
            for(var i = 1; i < hashParts.length; i++){
                pageState.push(hashParts[i]);
                var elem = $("#page_"+hashParts[i]);
                if(elem.length){
                    elem.addClass("active");
                }
            }
        }
        if(hash != '' && hash != '#'){
            setTimeout(function(){
                openPage(page, activePage);
            }, 300);
        }
    }

    function newPageState(str){
        var currentPage = $("#page_"+str),
            tmp = [];
        if(currentPage.length){
            var section = currentPage.parent();
            for(var i = 0; i< pageState.length; i++){
                if($('#page_'+pageState[i]).length && $('#page_'+pageState[i]).parent().attr("class") != section.attr("class")){
                    tmp.push(pageState[i]);
                } else {

                }
            }
            pageState = tmp;
            pageState.push(str);
        }
    }

    return {
        init: function(){
            var moving = false;
            $("body").on("mousewheel", function(e){
                e.preventDefault();
                e.stopPropagation();
                var delta = e.originalEvent.deltaY;
                if(!moving){
                    moving = true;
                    movePage(delta, function () {
                        setTimeout(function(){
                            moving = false;
                        }, 100);
                    });
                }
            });
            $("body").on("keydown", function(e){
                if(!moving){
                    moving = true;
                    if(e.which == "40"){
                        movePage(100, function () {
                            setTimeout(function(){
                                moving = false;
                            }, 100);
                        });
                    }
                    if(e.which == "38"){
                        movePage(-100, function () {
                            setTimeout(function(){
                                moving = false;
                            }, 100);
                        });
                    }
                }
            });
            $(".maincontent > a").on("click", function(e){
                e.preventDefault();
                var delta = 100;
                if($(this).attr("href")=='#top'){
                    delta = -100;
                }
                if(!moving) {
                    moving = true;
                    movePage(delta, function () {
                        setTimeout(function(){
                            moving = false;
                        }, 100);
                    });
                }
            });
            $("body").drag(function( ev, dd ){
                if(!moving) {
                    moving = true;
                    movePage(-dd.deltaY, function () {
                        setTimeout(function(){
                            moving = false;
                        }, 100);
                    });
                }
            },{ distance:50 });
            $(".page8_main .page8_buttons_link").on("click", function(e){
                e.preventDefault();
                var elem = $(this),
                    href = elem.attr("href");
                if(href=='#yes'){
                    $(".page8").addClass("have");
                    $(".page9_main").addClass("active");
                    setTimeout(function(){
                        openPage(8, 9);
                    }, 4000);
                } else {
                    $(".page8").removeClass("have");
                    $(".page8 > section").removeClass("active");
                    $(".page8_cat").addClass("active");
                }
            });
            var step3links = $(".page_content > a"),
                step3content = $(".page4 > section");
            step3links.on("click", function(e){
                e.preventDefault();
                var elem = $(this),
                    href = elem.attr("href"),
                    id = "#page_" + (href.replace("#",""));
                if(step3content.filter(".active").length){
                    var currentId = step3content.filter(".active").attr("id").replace("page_","");
                    document.location.hash = document.location.hash.replace("/"+currentId,"");
                }
                step3content.removeClass("active");
                $(id).addClass("active");
                newPageState(href.replace("#",""));
                openPage(3, 2);
            });
            $(".page9 a").on("click", function(e){
                e.preventDefault();
                openPage(9, 8);
            });
            $(".page10_again").on("click", function(e){
                e.preventDefault();
                openPage(0, 9);
                $(".maincontent > section").removeClass("loaded have");
                $(".maincontent > section > section").removeClass("active cat_final");
                $(".page8_main").addClass("active");
            });
            loadHash();
            var resize = setTimeout(function(){
                base.css({top: -pages.eq(activePage).position().top});
            }, 50);
            $(window).on("resize", function(){
                clearTimeout(resize);
                resize = setTimeout(function(){
                    base.css({top: -pages.eq(activePage).position().top});
                }, 50);
            });
            $(".page10 .page_bg").on("click", function(){
                openPage(8,9);
            });
        }
    }
};

(function () {
    navApp().init();
})();

//---------------navigation script END--------------//
