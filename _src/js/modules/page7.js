/**
 * Created by Verdi on 07.12.2014.
 */

//---------------page7 script BEGIN--------------//

var page6App = function(){
    var count = 0,
        counter = $(".page7 .page7_count_num span"),
        countLine = $(".page7 .page7_count"),
        counterFill = $(".page7 .page7_shape_fill");

    function changeCSS(){
        counter.text(count);
        countLine.css({
            bottom: count+"%"
        });
        counterFill.css({
            bottom: (count-100)+"%"
        });
    }

    return {
        init: function(){

        },
        countTo: function(num){
            setTimeout(function(){
                counter.removeClass("counting");
            }, 2000);
            if(!counter.hasClass("counting")){
                counter.addClass("counting");
                makeCount(count, num, 2000/(num-count), function(next){
                    count = next;
                    counter.text(count);
                    changeCSS();
                });
            }
        }
    }
};

(function () {
    page6App().init();
})();

//---------------page7 script END--------------//
